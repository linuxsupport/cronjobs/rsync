job "${PREFIX}_rsync_${NAME}" {
  datacenters = ["*"]

  type = "batch"

  periodic {
    cron = "${SCHEDULE}"
    time_zone = "Europe/Zurich"
    prohibit_overlap = true
  }

  priority = 40

  task "${PREFIX}_rsync_${NAME}" {
    driver = "docker"

    config {
      image = "https://gitlab-registry.cern.ch/linuxsupport/cronjobs/rsync/rsync:${CI_COMMIT_SHORT_SHA}"
      logging {
        config {
          tag = "${PREFIX}_rsync"
        }
      }
      volumes = [
        "$MOUNTPOINT:/repo",
        "/afs/cern.ch/project/gd/wlcg-repo:/wlcg-repo",
      ]
    }

    env {
      TOOL = "$TOOL"
      SOURCE = "$SOURCE"
      OPTIONS = "$OPTIONS"
      TAG = "${PREFIX}_rsync"
    }

    resources {
      cpu = 3000 # Mhz
      memory = 8192 # MB
    }

  }
}
