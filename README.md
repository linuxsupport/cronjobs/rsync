# rsync

This cronjob rsyncs Fedora and EPEL, among others. Access to the Fedora/EPEL tier1
mirror network is restricted and the IPs of the machines doing the rsync have to
be allowed to access. Should the IPs of one of these machines change, you'll get
"access denied" errors. You'll have to open a ticket to Fedora Infrastructure
for them to update the IPs. For example:

   * https://pagure.io/fedora-infrastructure/issue/9350
   * https://pagure.io/fedora-infrastructure/issue/11548
