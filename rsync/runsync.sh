#!/bin/bash

OUTPUT='/alloc/output.json'
LOCKFILE="/repo/.rsynclock-$NOMAD_TASK_NAME"

log () {
  mapfile IN
  cat << EOF | tr -d '\n'; echo
{ "repoid": "${NOMAD_TASK_NAME#prod_rsync_}",
  ${IN[@]}
}
EOF
}

error () {
  mapfile IN
  cat << EOF | log
    "message_type": "error",
    ${IN[@]}
EOF
}

# Let's make absolutely sure there's only one copy of the job running
if [[ -f "$LOCKFILE" ]]; then
  # The lockfile exists, let's check if it's fresh
  AGE=$((`date +%s` - `stat -c '%Y' $LOCKFILE`))
  if [[ $AGE -gt 86400 ]]; then
    echo "$LOCKFILE is over 24 hours old. It's probably stale, so get rid of it."
    rm -f "$LOCKFILE"
  fi
fi

# Grab the lock, exit if you fail
if !(set -o noclobber; echo "`hostname` ($$)" > "$LOCKFILE") 2> /dev/null; then
  echo "Lock exists: $LOCKFILE owned by $(cat $LOCKFILE)"
  exit
fi

# Delete the lock if we die
trap 'rm -f "$LOCKFILE"; exit $?' INT TERM EXIT

# Read the job parameters
if [[ "${TOOL}" == "rsync" ]]; then
  BINARY=/usr/bin/rsync
  # Make sure you NEVER EVER do an inplace rsync, it will completely break hard-links
  # and the workflows that will use these files later on. You've been warned!!
  OPTIONS="$OPTIONS --no-inplace"
elif [[ "${TOOL}" == "lftp" ]]; then
  BINARY=/usr/bin/lftp
elif [[ "${TOOL}" == "quick-fedora-mirror" ]]; then
  BINARY=/usr/bin/quick-fedora-mirror
  echo $OPTIONS | tr " " "\n" | sed 's/#/ /g' > /etc/quick-fedora-mirror.conf
  echo "DESTD=/repo" >> /etc/quick-fedora-mirror.conf
  echo "VERBOSE=8" >> /etc/quick-fedora-mirror.conf
  echo "REMOTE=$SOURCE" >> /etc/quick-fedora-mirror.conf
  echo "KEEPDIRTIMES=true" >> /etc/quick-fedora-mirror.conf
  echo "LOGITEMS=aeElrRs" >> /etc/quick-fedora-mirror.conf
  echo "RSYNC_PARTIAL_DIR_BUG=true" >> /etc/quick-fedora-mirror.conf
elif [[ "${TOOL}" == "ftpsync" ]]; then
  BINARY=/usr/bin/ftpsync
  mkdir -p /etc/ftpsync
  echo $OPTIONS | sed -r 's/[[:alnum:]_]+=/\n&/g' > /etc/ftpsync/ftpsync.conf
  echo "TO=/repo" >> /etc/ftpsync/ftpsync.conf
  echo "RSYNC_HOST=$SOURCE" >> /etc/ftpsync/ftpsync.conf
  echo "EXCLUDE=--exclude=${LOCKFILE#/repo/}" >> /etc/ftpsync/ftpsync.conf
else
  RET=-1
  cat << EOF | error | tee $OUTPUT
    "exit_code": $RET,
    "error": "tool not supported"
EOF
  exit $RET
fi

# Run rsync/lftp/quick-fedora-mirror
if [[ "${TOOL}" == "quick-fedora-mirror" ]]; then
  su - build -c "${BINARY} -T '1 week ago'" 2> /local/stderr
elif [[ "${TOOL}" == "ftpsync" ]]; then
  # ftpsync manages it's own lock files, delete it if it exists because it's probably stale
  rm -vf /repo/Archive-Update-in-Progress-*

  mkdir -p /alloc/log
  touch /alloc/log/rsync-ftpsync.log /alloc/log/rsync-ftpsync.error
  tail --follow --quiet /alloc/log/rsync-ftpsync.* &
  BASEDIR='/alloc' LOGNAME='/alloc/log' LOG='/dev/stdout' $BINARY 2> /local/stderr
else
  # If $SOURCE starts with /, let's check if it's empty or not
  if [[ $SOURCE == /* ]]; then
    if [[ -n "`find $SOURCE -maxdepth 0 -empty -print -quit`" ]]; then
      # It's empty, so /afs is probably not mounted correctly. Abort!
      RET=-1
      cat << EOF | error | tee $OUTPUT
      "exit_code": $RET,
      "error": "$SOURCE empty, refusing to run",
      "output": "$(sed 's/"/\\"/g' /local/stderr)"
EOF
      exit $RET
    fi
  fi
  $BINARY $OPTIONS $SOURCE /repo 2> /local/stderr
fi
RET=$?

# Check if there are leftover tmp directories, which will have to be deleted.
# For the ones that use quick-fedora-mirror, we kind of have to guess where the files go.
if [[ "${TOOL}" == "quick-fedora-mirror" ]]; then
  BASE="/repo/${NOMAD_TASK_NAME#prod_rsync_}"
else
  BASE="/repo"
fi
[[ -d $BASE ]] && find $BASE -type d -name '.~tmp~' -exec rm -rfv {} \;

if [[ $RET -ne 0 ]]; then
  cat << EOF | error | tee $OUTPUT
  "exit_code": $RET,
  "error": "sync failed",
  "output": "$(sed 's/"/\\"/g' /local/stderr)"
EOF
  exit $RET
fi

cat << EOF | log | tee $OUTPUT
  "message_type": "result",
  "exit_code": ${RET}
EOF

# Clean up after yourself, and release your trap
rm -f "$LOCKFILE"
trap - INT TERM EXIT
