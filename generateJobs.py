#!/usr/bin/python3

import sys
import os
import getopt
import yaml
from string import Template


try:
  opts, args = getopt.getopt(sys.argv[1:], "c:t:", ["config=", "template="])
except getopt.GetoptError:
  print('test.py -c <configfile> -t <templatefile>')
  sys.exit(1)

config_name = None
template_name = 'rsync.nomad.tpl'
for opt, arg in opts:
  if opt in ("-c", "--config"):
    config_name = arg
  elif opt in ("-t", "--template"):
    template_name = arg

if not config_name:
  print('Missing config file')
  sys.exit(1)

with open(config_name, 'r') as configfile:
  config = yaml.safe_load(configfile)

with open(template_name, 'r') as templatefile:
  template = Template(templatefile.read())

try:
  def_top = config['defaults']['top']
  def_tool = config['defaults']['tool']
  def_options = config['defaults']['options']
  def_schedule = config['defaults']['schedule']
except IndexError:
  print('Missing configuration options')
  sys.exit(2)

for c in config:
  if c == 'defaults':
    continue

  options = []
  options.extend(config[c].get('options', def_options))
  options.extend(config[c].get('extra_options', []))

  data = {
    'NAME'       : c,
    'TOOL'       : config[c].get('tool', def_tool),
    'SOURCE'     : config[c]['source'],
    'MOUNTPOINT' : '{0}/{1}'.format(config[c].get('top', def_top), config[c].get('destination', c)),
    'OPTIONS'    : ' '.join(options),
    'SCHEDULE'   : config[c].get('schedule', def_schedule),
  }

  # If the schedule is empty, the job is disabled
  if data['SCHEDULE'] == '':
    continue

  jobfile = '{0}_rsync_{1}.nomad'.format(os.getenv('PREFIX', 'dev'), c)
  with open(jobfile, 'w') as job:
    job.write(template.safe_substitute(data))
    print('Generated job: {0}'.format(jobfile))
